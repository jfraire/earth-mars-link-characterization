from agi.stk12.stkdesktop import STKDesktop
from agi.stk12.stkengine import STKEngine
from agi.stk12.stkobjects import *
from agi.stk12.stkobjects.astrogator import *
from agi.stk12.utilities.colors import *
from agi.stk12.stkutil import *
import csv
import math
import numpy as np

from comtypes.client import CreateObject
# from comtypes.gen import STKObjects

from datetime import datetime, timedelta

stkUI = AgVAStateCalcTrueLon

################
# Scenario
################
def createScenario(startDate, endDate):

    print("Starting STK")

    if stkUI:
        stk = STKDesktop.StartApplication(visible=True, userControl=True)
        m_root = stk.Root
    else:
        stk = STKEngine.StartApplication(noGraphics=True) 
        m_root = stk.NewObjectRoot()

    print("Creating Scenario (start: {}, end {})".format(startDate, endDate))

    m_root.NewScenario('interplanetary-scenario')

    dimensions = m_root.UnitPreferences
    dimensions.SetCurrentUnit("Time", "sec")
    dimensions.SetCurrentUnit("DateFormat", "EpSec")

    startTime = m_root.ConversionUtility.NewDate("UTCG", startDate)
    stopTime = m_root.ConversionUtility.NewDate("UTCG", endDate)

    IAgScenario(m_root.CurrentScenario).SetTimePeriod(startTime.Format("EpSec"), stopTime.Format("EpSec"))
    IAgScenario(m_root.CurrentScenario).Epoch = startTime.Format("EpSec")
    IAgScenario(m_root.CurrentScenario).Animation.TimePeriod.UseAnalysisStartTime = True

    return stk, m_root


################
# GS
################
def createLander(m_root, gsParams):
    print("Creating Lander: {}".format(gsParams['Name']))

    centralBody = gsParams['Body']
    gs = m_root.CurrentScenario.Children.NewOnCentralBody(AgESTKObjectType.eFacility, gsParams['Name'], centralBody)
    gs.UseTerrain = False
    planetodetic = gs.Position.ConvertTo(AgEPositionType.ePlanetodetic)
    planetodetic.Lat = gsParams['Lat']
    planetodetic.Lon = gsParams['Lon']
    planetodetic.Alt = gsParams['Alt']
    gs.Position.Assign(planetodetic)

    if "MinElev" in gsParams:
        # Constraint: elevation
        elev_constraint = gs.AccessConstraints.AddConstraint(AgEAccessConstraints.eCstrElevationAngle)
        elev_constraint.EnableMin = True
        elev_constraint.Min = gsParams['MinElev']  


################
# DSN
################
def createDSN(m_root):

    # Goldstone Deep Space Communications Complex, California, USA
    goldstone_lat = 35.4266  # Latitude in degrees
    goldstone_lon = -116.8899  # Longitude in degrees
    goldstone_alt = 1.0  # Altitude in kilometers

    # Madrid Deep Space Communications Complex, Spain
    madrid_lat = 40.4316  # Latitude in degrees
    madrid_lon = -4.2491  # Longitude in degrees
    madrid_alt = 0.8  # Altitude in kilometers

    # Canberra Deep Space Communication Complex, Australia
    canberra_lat = -35.4020  # Latitude in degrees
    canberra_lon = 148.9819  # Longitude in degrees
    canberra_alt = 0.7  # Altitude in kilometers

    dsnParams1 = {
        "Name": "DSN1",
        "Body": "Earth",
        "Lat": goldstone_lat,
        "Lon": goldstone_lon,
        "Alt": goldstone_alt,
    }
    createLander(m_root, dsnParams1)

    dsnParams2 = {
        "Name": "DSN2",
        "Body": "Earth",
        "Lat": madrid_lat,
        "Lon": madrid_lon,
        "Alt": madrid_alt,
    }
    createLander(m_root, dsnParams2)

    dsnParams3 = {
        "Name": "DSN3",
        "Body": "Earth",
        "Lat": canberra_lat,
        "Lon": canberra_lon,
        "Alt": canberra_alt,
    }
    createLander(m_root, dsnParams3)

    return [dsnParams1, dsnParams2, dsnParams3]


################
# Orbiter
################
def createOrbiter(m_root, satParams):
    print("Creating orbiter: {}".format(satParams['Name']))

    centralBody = satParams['Body']
    leo = IAgSatellite(m_root.CurrentScenario.Children.NewOnCentralBody(AgESTKObjectType.eSatellite, satParams['Name'], centralBody))
    leo.Graphics.SetAttributesType(AgEVeGfxAttributes.eAttributesBasic)
    attributes = IAgVeGfxAttributesBasic(leo.Graphics.Attributes)
    attributes.Color = Colors.FromRGB(134, 217, 72)
    # Set Propagator
    leo.SetPropagatorType(AgEVePropagatorType.ePropagatorTwoBody)
    twobody = IAgVePropagatorTwoBody(leo.Propagator)
    classical = IAgOrbitStateClassical(twobody.InitialState.Representation.ConvertTo(AgEOrbitStateType.eOrbitStateClassical))
    # Coordinate system
    # classical.CoordinateSystemType = AgECoordinateSystem.eCoordinateSystemJ2000
    classical.CoordinateSystemType = AgECoordinateSystem.eCoordinateSystemInertial
    smartInterval = twobody.EphemerisInterval
    smartInterval.SetExplicitInterval(IAgScenario(m_root.CurrentScenario).StartTime, IAgScenario(m_root.CurrentScenario).StopTime)
    twobody.Step = 60
    # Location
    classical.LocationType = AgEClassicalLocation.eLocationTrueAnomaly
    trueAnomaly = IAgClassicalLocationTrueAnomaly(classical.Location)
    trueAnomaly.Value = satParams['TrAnom'] 
    # SizeShape
    # classical.SizeShapeType = AgEClassicalSizeShape.eSizeShapeAltitude
    # altitude = IAgClassicalSizeShapeAltitude(classical.SizeShape)
    # altitude.ApogeeAltitude = satParams['Alt'] 
    # altitude.PerigeeAltitude = satParams['Alt']
    classical.SizeShapeType = AgEClassicalSizeShape.eSizeShapeSemimajorAxis
    altitude = IAgClassicalSizeShapeSemimajorAxis(classical.SizeShape)
    altitude.SemiMajorAxis = satParams['Smaj'] 
    altitude.Eccentricity = satParams['Ecc']
    # Orientation
    classical.Orientation.ArgOfPerigee = satParams['ArgPer'] 
    classical.Orientation.Inclination = satParams['Inc'] 
    classical.Orientation.AscNodeType = AgEOrientationAscNode.eAscNodeRAAN
    raan = IAgOrientationAscNodeRAAN(classical.Orientation.AscNode)
    raan.Value = satParams['RAAN'] 
    twobody.InitialState.Representation.Assign(classical)
    twobody.Propagate()

    if "MaxRange" in satParams:
        # Define the maximum range constraint 
        cstrRange = IAgAccessCnstrMinMax(leo.AccessConstraints.AddConstraint(AgEAccessConstraints.eCstrRange))
        cstrRange.EnableMax = True
        cstrRange.Max = satParams['MaxRange']


################
# 16 Mars Orbiters
################
def create16MarsOrbiters(m_root):

    orbiterParams = []

    alts = [100, 500, 900, 1300]
    incs = [0  , 30 , 60 , 90  ]

    for alt in alts:
        for inc in incs:

            smj = 3396 + alt # Semi-major axis (km)
            ecc = 0          # Eccentricity
            inc = inc        # Inclination (deg)
            tam = 0          # True Anomaly (deg)
            ran = 0          # RAAN (deg)
            ape = 0          # Argument of Periapsis (deg)
            satParams = {
                "Name": "SAT-Mars-alt{}-inc{}".format(alt, inc),
                "Body": "Mars",
                "Smaj": smj,
                "Ecc": ecc,
                "Inc": inc,
                "RAAN": ran,
                "ArgPer": ape,
                "TrAnom": tam,
            }
            createOrbiter(m_root, satParams)
            orbiterParams.append(satParams)

    return orbiterParams


################
# Access
################
def computeCoordinates(m_root, satParams1, timeStep=10):
    print("Computing Coordinates: {}".format(satParams1['Name']))

    llo = IAgStkObject(m_root.CurrentScenario.Children[satParams1['Name']]) # IAgSatellite

    # Create a data provider for fixed coordinates (Earth Fixed)
    dpF = IAgDataPrvTimeVar(IAgDataProviderGroup(llo.DataProviders["Vectors(Fixed)"]).Group["Position(Moon)"])

    # Create a data provider for inertial coordinates (Inertial True of Date)
    dpI = IAgDataPrvTimeVar(IAgDataProviderGroup(llo.DataProviders["Vectors(ICRF)"]).Group["Position(Moon)"])

    startTimeUTCG, stopTimeUTCG = m_root.CurrentScenario.StartTime, m_root.CurrentScenario.StopTime

    # Get the x, y, z coordinates in fixed and inertial frames
    coordsF = IAgDrResult(dpF.Exec(startTimeUTCG, stopTimeUTCG, timeStep))
    coordsI = IAgDrResult(dpI.Exec(startTimeUTCG, stopTimeUTCG, timeStep))

    # Extract the x, y, z values from the result
    tF = coordsF.DataSets.GetDataSetByName('Time').GetValues()
    xF = coordsF.DataSets.GetDataSetByName('x').GetValues()
    yF = coordsF.DataSets.GetDataSetByName('y').GetValues()
    zF = coordsF.DataSets.GetDataSetByName('z').GetValues()

    tI = coordsI.DataSets.GetDataSetByName('Time').GetValues()
    xI = coordsI.DataSets.GetDataSetByName('x').GetValues()
    yI = coordsI.DataSets.GetDataSetByName('y').GetValues()
    zI = coordsI.DataSets.GetDataSetByName('z').GetValues()

    file_name = "coords_if_{}".format(satParams1['Name'])

    # Write output access CSV
    satParams = []
    # Add parameters from satParams1
    for key, value in satParams1.items():
        satParams.append({"Parameter": "1" + key, "Value": value})

    # Append the satellite parameters to the CSV file
    with open(file_name + ".csv", "w", newline="") as file:
        writer = csv.DictWriter(file, fieldnames=["Parameter", "Value"])
        for param_dict in satParams:
            writer.writerow(param_dict)
        file.close()

    # Append position time-evolving data to the CSV file
    with open(file_name + ".csv", mode='a', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(['Time[EpSec]', 'xF[km]', 'yF[km]', 'zF[km]', 'xI[km]', 'yI[km]', 'zI[km]'])

        # Write the coordinates line by line
        for row in zip(tF, xF, yF, zF, xI, yI, zI):
            writer.writerow(row)


################
# Access AER
################
def computeAccessAER(m_root, satParams1, satParams2, timeStep=10):

    print("Computing Access: {} to {}".format(satParams1['Name'], satParams2['Name']))
    access = IAgStkAccess(m_root.CurrentScenario.Children[satParams1['Name']].GetAccessToObject(m_root.CurrentScenario.Children[satParams2['Name']]))
    access.ComputeAccess()

    dpAER = IAgDataPrvTimeVar(IAgDataProviderGroup(access.DataProviders["AER Data"]).Group["Default"])
    dataPrvElementsLink = [ "Time", "Azimuth", "Elevation", "Range", "AzimuthRate", "ElevationRate", "RangeRate" ]
    accessIntervals = access.ComputedAccessIntervalTimes

    report = "Time[EpSec],Azimuth[deg],Elevation[deg],Range[km],AzimuthRate[deg/s],ElevationRate[deg/s],RangeRate[km/s]\n"

    accessIntervals.Count

    for a in range(accessIntervals.Count):

        startTimeUTCG, stopTimeUTCG = accessIntervals.GetInterval(a)
        results = IAgDrResult(dpAER.ExecElements(startTimeUTCG, stopTimeUTCG, timeStep, dataPrvElementsLink))

        dpTime = results.DataSets[0].GetValues()
        dpAzimuth = results.DataSets[1].GetValues()
        dpElevation = results.DataSets[2].GetValues()
        dpRange = results.DataSets[3].GetValues()
        dpAzimuthRate = results.DataSets[4].GetValues()
        dpElevationRate = results.DataSets[5].GetValues()
        dpRangeRate = results.DataSets[6].GetValues() 

        for t, _ in enumerate(dpTime):
            report += "{},{},{},{},{},{},{}\n".format(dpTime[t], dpAzimuth[t], dpElevation[t], dpRange[t], dpAzimuthRate[t], dpElevationRate[t], dpRangeRate[t])

    # Write output access CSV
    satParams = []
    # Add parameters from satParams1
    for key, value in satParams1.items():
        satParams.append({"Parameter": "1" + key, "Value": value})
    # Add parameters from satParams2
    for key, value in satParams2.items():
        satParams.append({"Parameter": "2" + key, "Value": value})

    file_name = "access_aer_{}_to_{}".format(satParams1['Name'], satParams2['Name'])
    
    # Append the satellite parameters to the CSV file
    with open(file_name + ".csv", "w", newline="") as file:
        writer = csv.DictWriter(file, fieldnames=["Parameter", "Value"])
        for param_dict in satParams:
            writer.writerow(param_dict)
        file.close()
    
    # Append access time-evolving data to the CSV file
    with open(file_name + ".csv", "a") as file:
        file.write(report)
        file.close()


################
# Access INT
################
def computeAccessINT(m_root, params1, params2, timeStep=10, label=""):

    report = ""

    print("Computing Access: {} to {}".format(params1['Name'], params2['Name']))
    access = IAgStkAccess(m_root.CurrentScenario.Children[params1['Name']].GetAccessToObject(m_root.CurrentScenario.Children[params2['Name']]))
    access.ComputeAccess()

    dpAER = IAgDataPrvTimeVar(IAgDataProviderGroup(access.DataProviders["AER Data"]).Group["Default"])
    dataPrvElementsLink = ["Range"]
    accessIntervals = access.ComputedAccessIntervalTimes

    startTimeEpSec = IAgScenario(m_root.CurrentScenario).StartTime
    stopTimeEpSec = IAgScenario(m_root.CurrentScenario).StopTime
    startTime = m_root.ConversionUtility.NewDate("EpSec", "{}".format(startTimeEpSec))
    stopTime = m_root.ConversionUtility.NewDate("EpSec", "{}".format(stopTimeEpSec))
    report += "Start Date (EpSec={}): {}\n".format(startTimeEpSec, startTime.Format("UTCG"))
    report += "Stop Date (EpSec={}): {}\n".format(stopTimeEpSec, stopTime.Format("UTCG"))
    report += "Access,startTime[EpSec],stopTime[EpSec],Duration[Sec],avgRange[km]\n"

    for a, i in enumerate(range(accessIntervals.Count)):

        startTimeEpSec, stopTimeEpSec = accessIntervals.GetInterval(a)
        # startTime = m_root.ConversionUtility.NewDate("EpSec", "{}".format(startTimeEpSec))
        # stopTime = m_root.ConversionUtility.NewDate("EpSec", "{}".format(stopTimeEpSec))

        duration = stopTimeEpSec - startTimeEpSec  
        #duration = float(stopTime.Format("EpSec")) - float(startTime.Format("EpSec"))      

        results = IAgDrResult(dpAER.ExecElements(startTimeEpSec, stopTimeEpSec, timeStep, dataPrvElementsLink))
        dpRange = results.DataSets[0].GetValues()

        report += "{},{},{},{},{}\n".format(i, startTimeEpSec, stopTimeEpSec, duration, np.mean(dpRange))
 
    # Write output access CSV
    allParams = []
    # Add parameters from satParams1
    for key, value in params1.items():
        allParams.append({"Parameter": "1" + key, "Value": value})
    # Add parameters from satParams2
    for key, value in params2.items():
        allParams.append({"Parameter": "2" + key, "Value": value})

    file_name = "access_int_{}_{}_to_{}".format(label, params1['Name'], params2['Name'])
    
    # Append the satellite parameters to the CSV file
    with open(file_name + ".csv", "w", newline="") as file:
        writer = csv.DictWriter(file, fieldnames=["Parameter", "Value"])
        for param_dict in allParams:
            writer.writerow(param_dict)
        file.close()
    
    # Append access time-evolving data to the CSV file
    with open(file_name + ".csv", "a") as file:
        file.write(report)
        file.close()


def addSensorToOrbiters(m_root, satParams):

    cone_half_angle = 20

    for satParam in satParams:
        # Access the orbiter object
        orbiter = m_root.CurrentScenario.Children[satParam['Name']]
        # Create a new sensor
        sensor = IAgSensor(orbiter.Children.New(AgESTKObjectType.eSensor, "Sensor"))
        # Set Sensor cone
        sensor.CommonTasks.SetPatternSimpleConic(cone_half_angle, 5) 
    

def create4MarsLandersLatitude(m_root):

    lndParams = []

    for lat in [0, 30, 60, 90]:
        lndParam = {
            "Name": "LND-Mars-lat{}".format(lat),
            "Body": "mARS",
            "Lat": lat,
            "Lon": 0,
            "Alt": 0,
        }
        createLander(m_root, lndParam)
        lndParams.append(lndParam)

    return lndParams


################
# Main
################
def main():

    # Synodic period of Earth and Mars: 780 days (about 2.1 years)
    start_date_str = "01 Jan 2025 00:00:00.000"
    start_date = datetime.strptime(start_date_str, "%d %b %Y %H:%M:%S.%f")
    stop_date = start_date + timedelta(days = (365))

    # Convert datetime objects to strings in the desired format
    startDate = start_date.strftime("%d %b %Y %H:%M:%S.%f")
    endDate = stop_date.strftime("%d %b %Y %H:%M:%S.%f")

    stk, m_root = createScenario(startDate, endDate)

    lndParams = create4MarsLandersLatitude(m_root)
    satParams = create16MarsOrbiters(m_root)

    for lndParam in lndParams:
        for satParam in satParams:
            computeAccessINT(m_root, lndParam, satParam, label="1")

    input("Press enter to close the application...")
    stk.ShutDown()


################
# Main
################
if __name__ == "__main__":
    main()